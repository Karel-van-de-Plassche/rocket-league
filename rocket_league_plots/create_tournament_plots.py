import os.path

import pandas as pd

from bokeh.models import TabPanel, Tabs, Tooltip, Button, DataTable, ColumnDataSource, TableColumn, DateFormatter
from bokeh.models.layouts import Row
from bokeh.layouts import column, row
from bokeh.plotting import figure, show, curdoc
#from bokeh.widgets import DataTable

from googleapiclient.discovery import build
from google.oauth2 import service_account
import google.auth
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

#SCOPES = [
#    'https://www.googleapis.com/auth/spreadsheets',
#    'https://www.googleapis.com/auth/drive'
#]
SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"]


# The ID and range of a sample spreadsheet.
spreadsheet_id = "1RUQln5lXRM8EDd3Pxstt8KPmAhzddUnXfSg27uzVnUk"
range_name = "A:G"

def get_tournament_data():
    creds = None
    creds = service_account.Credentials.from_service_account_file('credentials.json', scopes=SCOPES)
    try:
        spreadsheet_service = build('sheets', 'v4', credentials=creds)
        drive_service = build('drive', 'v3', credentials=creds)
        result = (
            spreadsheet_service.spreadsheets()
            .values()
            .get(spreadsheetId=spreadsheet_id, range=range_name)
            .execute()
        )
        rows = result.get("values", [])
        print(f"{len(rows)} rows retrieved")
        return result
    except HttpError as error:
        print(f"An error occurred: {error}")
        return error

def parse_rows(rows):
    df = pd.DataFrame(rows["values"][1:], columns=rows["values"][0])
    df["Datum"] = pd.to_datetime(df["Datum"], format="%d %B %Y")
    df["Season No"] = df["Season No"].astype(int)
    df["2nd chance"] = df["2nd chance"] == "1"
    df["Result"] = df["Result"].astype(int)
    return df

def create_layout(df):
    #from IPython import embed; embed()
    data = df.to_dict("list")
    source = ColumnDataSource(data)
    # Manual column widths.. See
    # https://github.com/bokeh/bokeh/issues/10512
    # https://github.com/bokeh/bokeh/issues/13460
    columns = [
                TableColumn(field="Season No", title="Season No", width=90),
                TableColumn(field="Datum", title="Datum", formatter=DateFormatter(), width=90),
                TableColumn(field="Time", title="Time", width=60),
                TableColumn(field="Rank", title="Rank", width=90),
                TableColumn(field="2nd chance", title="2nd chance", width=90),
                TableColumn(field="Result", title="Result", width=60),
                TableColumn(field="Notes", title="Notes", width=300),
            ]
    total_width = sum([col.width for col in columns]) + 80 #Plus margins ofc
    data_table = DataTable(source=source, columns=columns, autosize_mode="none", width=total_width)
    #data_table = DataTable(source=source, columns=columns, autosize_mode="force_fit", width=1200)
    #data_table = DataTable(source=source, columns=columns, autosize_mode="fit_viewport", background="yellow")
    #data_table = DataTable(source=source, columns=columns, autosize_mode="fit_columns", background="yellow")
    p1 = figure(width=300, height=300)
    p1.scatter([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], size=20, color="navy", alpha=0.5)

    p2 = figure(width=300, height=300)
    p2.line([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], line_width=3, color="navy", alpha=0.5)

    tabs = Tabs(tabs=[
            TabPanel(child=row(data_table), title="Data Selector"),
            TabPanel(child=p2, title="line"),
    ],
                background="yellow",
                )
    layout = column(tabs)
    return layout



rows = get_tournament_data()
df = parse_rows(rows)
if __name__ == "__main__":
    layout = create_layout(df)
    #from IPython import embed; embed()
else:
    # Bokeh serve
    layout = create_layout(df)
    curdoc().add_root(layout)
