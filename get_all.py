import requests
from html.parser import HTMLParser
from ast import literal_eval
from collections import UserDict
from pathlib import Path
import pandas as pd
import numpy as np

from IPython import embed
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

FORBIDDEN_DATA = [
    "You can help improve this page!",
    "Camera settings quickly become outdated. To fix this issue, please help keep the list up to date by going to player pages and updating their settings if needed.",
]


class CameraSettingsParser(HTMLParser):
    itemdict = {}
    reading_table = False
    reading_header = False
    header = []
    table_tmp = []
    table_line = {}
    reading_td = False
    data = {}
    current_label = None
    current_player = None
    name_field = []
    tables = []

    def handle_starttag(self, tag, attrs):
        if tag == "table":
            self.reading_table = True
            print(f"start.tag = '{tag}', '{attrs}'")
        if self.reading_table and tag == "td":
            self.reading_td = True
            for attr in attrs:
                if attr[0] == "data-label":
                    if attr[1] == "Player" and len(self.table_line) > 0:
                        self.table_tmp.append(self.table_line)
                        self.table_line = {}
                    self.current_label = attr[1]
                else:
                    self.current_label = None
        if tag == "a" and self.reading_td:
            # Team always times three
            # Name always once
            # So:
            # Name + 3x Team -> next name
            # or
            # Name -> next name
            # For self.table_line == {None: ''}
            if self.table_line == {None: ""}:
                self.table_line["Player"] = dict(attrs)["title"]
            elif "Player" in self.table_line:
                self.table_line["Team"] = dict(attrs)["title"]

    def handle_endtag(self, tag):
        # 0ver Zer0
        # 1zen
        if tag == "table":
            self.reading_table = False
            print(f"endtag = '{tag}'")
            df = pd.DataFrame(self.table_tmp)
            df = df.where(lambda x: x != "", np.NaN)
            df = df.dropna(axis=0, how="all")
            df = df.dropna(axis=1, how="all")
            if df.size == 0:
                # Empty table
                self.table_tmp = []
            else:
                # This happens only once
                self.tables.append(df)
                self.table_tmp = []
        elif tag == "td":
            self.reading_td = False

    def handle_data(self, data):
        # if data == "":
        #    from IPython import embed; embed()
        #    return
        rdata = data.strip()
        # 12 columns
        if self.current_label == "Player":
            from IPython import embed

            embed()
        if self.reading_table:
            if rdata not in FORBIDDEN_DATA:
                if self.reading_td:
                    print(self.current_label)
                    print(rdata)
                    self.table_line[self.current_label] = rdata
        return
        if data.startswith(ITEM_LIST_PRESTRING):
            to_be_parsed = data[len(ITEM_LIST_PRESTRING) :]
            ii = to_be_parsed.find("{")
            itemdict = to_be_parsed[ii:]
            itemdict = itemdict[: itemdict.rfind("\nvar") - 2].strip()
            itemdict = itemdict.splitlines()[0]
            # This is different for gather professions
            is_gather_profession = itemdict[-1] == "}" and itemdict[-2] == "}"
            if not is_gather_profession:
                itemdict = itemdict[:-2]
            # I do not think there is a reliable way to check if we got all
            # items requested. From manual checks looks fine
            self.itemdict = literal_eval(itemdict)
            logger.info("Parsed data")


LIQUIPEDIA_URL = "https://liquipedia.net"
ROCKET_LEAGUE_URI = "rocketleague"
CAMERA_URI = "List_of_player_camera_settings"

local_file = Path("page.html")


def get_map_list():
    parser = CameraSettingsParser()
    if local_file.exists() or False:
        logger.info(f"Using local file '{local_file}'")
        with open(local_file, "r") as f_:
            lines = f_.readlines()
            text = "".join(lines)
            parser.feed(text)
    else:
        get_url = f"{LIQUIPEDIA_URL}/{ROCKET_LEAGUE_URI}/{CAMERA_URI}"
        logger.info(f"Getting {get_url}")
        rr = requests.get(get_url)
        if rr.status_code == 200:
            parser.feed(rr.text)

        with open(local_file, "w") as f_:
            f_.writelines(rr.text)
    return parser


if __name__ == "__main__":
    pp = get_map_list()
